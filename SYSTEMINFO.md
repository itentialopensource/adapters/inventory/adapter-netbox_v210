# NetBox v2.10

Vendor: NetBox Labs
Homepage: https://netboxlabs.com/

Product: NetBox
Product Page: https://netboxlabs.com/oss/netbox/

## Introduction
We classify NetBox into the Inventory domain as NetBox contains “Trusted Authoritative Information” on network devices and configuration. We also classify it into the Network Services domain because it provides functionality for managing network configuration, such as devices interfaces, VLANS, etc.

"NetBox serves as a central repository for tracking and documenting network devices, circuits, and other components"

"NetBox helps network engineers and administrators manage and document their network infrastructure, which comprises devices, circuits, IP addresses, and VLANs"

## Why Integrate
The NetBox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with NetBox.

With this adapter you have the ability to perform operations with NetBox such as:

- Create Subnet
- Create IP Record
- Get Next Available IP
- Get Circuit
- Get L2VPN
- Get Device

## Additional Product Documentation
The [API documents for NetBox](https://netbox.grid.uchicago.edu/api/schema/swagger-ui/)