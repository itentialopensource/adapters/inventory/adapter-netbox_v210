
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:24PM

See merge request itentialopensource/adapters/adapter-netbox_v210!17

---

## 0.5.3 [09-12-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-netbox_v210!15

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:35PM

See merge request itentialopensource/adapters/adapter-netbox_v210!14

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_19:47PM

See merge request itentialopensource/adapters/adapter-netbox_v210!13

---

## 0.5.0 [05-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!12

---

## 0.4.5 [03-26-2024]

* Changes made at 2024.03.26_14:44PM

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!11

---

## 0.4.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!10

---

## 0.4.3 [03-13-2024]

* Changes made at 2024.03.13_14:02PM

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!9

---

## 0.4.2 [03-11-2024]

* Changes made at 2024.03.11_14:11PM

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!8

---

## 0.4.1 [02-26-2024]

* Changes made at 2024.02.26_13:39PM

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!7

---

## 0.4.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!6

---

## 0.3.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!4

---

## 0.2.1 [03-07-2022]

- Fix graphql sendGetBody
- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!3

---

## 0.2.0 [02-14-2022]

- Added new call for graphql

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!2

---

## 0.1.2 [12-30-2021]

- Add / to generic call paths

See merge request itentialopensource/adapters/inventory/adapter-netbox_v210!1

---

## 0.1.1 [08-02-2021]

- Initial Commit

See commit 1173452

---
